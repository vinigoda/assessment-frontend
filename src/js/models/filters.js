window.filterCollection = {};


export var filter = {
    create: function (key, title) {

        window.filterCollection[key] = { title: title, options: [] };

    },

    addOptions: function (key, option) {

        var options = window.filterCollection[key].options;

        if (!options.includes(option)) options.push(option);

    },

    print: function () {
        var filters = Object.keys(window.filterCollection);

        filters.forEach(function (keyFilter) {

            var filter = window.filterCollection[keyFilter];

            var h6 = document.createElement('h6');
            var ul = document.createElement('ul');

            h6.innerHTML = filter.title;

            filter.options.forEach(function (option) {
                var li = document.createElement('li');

                li.addEventListener('click', function () {
                    var filtred = window.productCollection.filter(function (product) {
                        

                        return product.filter[0][keyFilter] == option;
                    })


                    document.getElementById('prod').innerHTML = '';
                    filtred.forEach(function (f) {
                        f['to'+window.productCollectionStyle]('prod');
                    })
                });

                li.innerHTML = option;
                ul.appendChild(li);
            })

            document.getElementById('filter').appendChild(h6);
            document.getElementById('filter').appendChild(ul);

        })

    },

    reset: function () {
        window.filterCollection = {};
        document.getElementById('filter').innerHTML = '';

    }
}

