

import {displayProducts} from '../productsController';

// import Product from './product.js';


export default class Menu {

    constructor(data) {
        this.id = data.id;
        this.name = data.name;
    }

    toMenu(idTag) {

        let id = this.id;
        let name = this.name;
        let breadcrumb = this.toBreadcrumb;

        var a = document.createElement("a");
        a.href = "#";

        //MENU
        a.addEventListener('click', function () {
            displayProducts(id);
            breadcrumb(name);

            var gridStyle = document.getElementById('grid');
            var listStyle = document.getElementById('list');

            gridStyle.setAttribute('idList', id);
            listStyle.setAttribute('idList', id);
        });

        a.innerHTML = name.toUpperCase();
        document.getElementById(idTag).appendChild(a);

    }



    toBreadcrumb(name) {

        //title
        var title = document.getElementById('title');
        title.innerHTML = '';
        title.innerHTML = name;
    
        //BreadCrumb
        var breadcrumb = document.getElementById('bread');
        var arrow = document.createElement("span");
        var page = document.createElement("span");
    
        arrow.innerHTML = '>';
        page.innerHTML = name;
    
        breadcrumb.innerHTML = '';
        breadcrumb.appendChild(arrow);
        breadcrumb.appendChild(page);
    }

};




