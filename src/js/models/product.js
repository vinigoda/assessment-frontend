
export default class Product {

    constructor(data) {
        this.id = data.id;
        this.sku = data.sku;
        this.path = data.path;
        this.name = data.name;
        this.image = data.image;
        this.price = data.price;
        this.specialPrice = data.specialPrice ? data.specialPrice : 0;
        this.filter = data.filter;
    }

    toGrid(idTag) {

        var col = document.createElement("div");
        var box = document.createElement("div");
        var img = document.createElement("img");
        var productName = document.createElement("h6");
        var prices = document.createElement("div");

        var price = document.createElement("span");
        var btn = document.createElement("button");


        col.className = 'col-lg-3 col-md-4 col-sm-6  d-flex flex-column align-items-center products';
        box.className = 'box';       
        productName.className = 'text-center';
        prices.className = 'price w-100 d-flex justify-content-around';
        btn.className = 'btn btn-block btn-primary';
        
        img.className = 'product-img';
        img.alt = this.name;
        img.title = this.name;
        img.src = this.image;



        productName.innerHTML = this.name;
        btn.innerHTML = 'COMPRAR';

        if (this.specialPrice > 0) {

            var oldPrice = document.createElement("span");
            oldPrice.className = 'oldPrice';
            prices.appendChild(oldPrice);

            oldPrice.innerHTML = 'R$ ' + this.price.toFixed(2).replace(".", ",");
            price.innerHTML = 'R$ ' + this.specialPrice.toFixed(2).replace(".", ",");
        } else {
            price.innerHTML = 'R$ ' + this.price.toFixed(2).replace(".", ",");
        }


        box.appendChild(img);

        prices.appendChild(price);



        col.appendChild(box);
        col.appendChild(productName);
        col.appendChild(prices);
        col.appendChild(btn);


        document.getElementById(idTag).appendChild(col);

    }


    toList(idTag) {

        var col = document.createElement("div");
        var box = document.createElement("div");
        var img = document.createElement("img");
        var infos = document.createElement("div");
        var productName = document.createElement("h6");
        var prices = document.createElement("div");
        var price = document.createElement("span");
        var btn = document.createElement("button");


        col.className = 'col-md-12 d-flex  align-items-center justify-content-around products';
        box.className = 'box';
        infos.className = 'infos';
        productName.className = 'text-center';
        prices.className = 'price w-100 d-flex justify-content-around';
        btn.className = 'btn btn-block btn-primary';

        img.className = 'product-img';
        img.alt = this.name;
        img.title = this.name;
        img.src = this.image;


        productName.innerHTML = this.name;
        btn.innerHTML = 'COMPRAR';

        if (this.specialPrice > 0) {

            var oldPrice = document.createElement("span");
            oldPrice.className = 'oldPrice';
            prices.appendChild(oldPrice);

            oldPrice.innerHTML = 'R$ ' + this.price.toFixed(2).replace(".", ",");
            price.innerHTML = 'R$ ' + this.specialPrice.toFixed(2).replace(".", ",");
        } else {
            price.innerHTML = 'R$ ' + this.price.toFixed(2).replace(".", ",");
        }


        box.appendChild(img);

        prices.appendChild(price);



        col.appendChild(box);

        infos.appendChild(productName);
        infos.appendChild(prices);
        infos.appendChild(btn);

        col.appendChild(infos);

        document.getElementById(idTag).appendChild(col);

    }


}



