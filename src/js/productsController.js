import Product from './models/product.js';
import { filter } from './models/filters'

export function displayProducts(id = 1) {
    window.productCollection = [];
    document.getElementById('prod').innerHTML = '';


    filter.reset();



    var db;


    // Realiza a requisição na API

    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {

            db = JSON.parse(xhttp.responseText)
            // console.log(xhttp.responseText);
        }
    };
    xhttp.open("GET", "http://localhost:8888/api/V1/categories/" + id, false);
    xhttp.send();


    //Separa os possiveis filtros
    db.filters.forEach(function (v) {

        var key = Object.keys(v);

        filter.create(key[0], v[key[0]]);

    })


    //Adicionas os produtos ao DOM, e pega as possiveis opções de filtro de cada produto
    let products = db.items;
    products.forEach(function (v) {

        let product = new Product(v);

        product['to' + window.productCollectionStyle]('prod');

        window.productCollection.push(product);

        product.filter.forEach(function (f) {

            var key = Object.keys(f);

            filter.addOptions(key[0], f[key[0]]);

        })



    });


    //deposi de montar o obj de filtro renderizo as opçoes na sideBar
    filter.print();


    //Muda o Layout de exibição dos produtos
    var gridStyle = document.getElementById('grid');
    var listStyle = document.getElementById('list');


    gridStyle.addEventListener('click', function () {

        listStyle.className = '';
        gridStyle.className = 'active';
        document.getElementById('prod').innerHTML = '';
        window.productCollectionStyle = 'Grid';

        window.productCollection.forEach(function (p) {

            p['to' + window.productCollectionStyle]('prod');
        })
    });

    listStyle.addEventListener('click', function () {
        gridStyle.className = '';
        listStyle.className = 'active';
        document.getElementById('prod').innerHTML = '';
        window.productCollectionStyle = 'List';

        window.productCollection.forEach(function (p) {

            p['to' + window.productCollectionStyle]('prod');

        })
    });
}




