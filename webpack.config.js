const path = require('path');


module.exports = {
  watch: true,
  entry: './src/js/index.js',
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'public/dist'),
    
    // library: 'test',
    // libraryTarget: 'window',
    // libraryExport: 'default'
  },
  module: {
    rules: [
      {
        test: /\.s[ac]ss$/i,
        use: [
          'style-loader',
          'css-loader',
          'sass-loader',
        ]
      },
      {
        test: /\.(png|woff|woff2|eot|ttf|svg)$/,
        use: [
          {
            loader: 'url-loader?limit=100000',
            options: {
              name: '[name].[ext]',
              outputPath: 'main.js'
            }
          }
        ]

      }
    ]
  }
};